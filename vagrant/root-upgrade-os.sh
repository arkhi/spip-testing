#! /bin/bash
#
# This file is about upgrading the OS with the root user.

apt-get update && apt-get upgrade -y && apt-get autoremove && apt-get autoclean;