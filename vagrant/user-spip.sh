#! /bin/bash

ROOT_PUBLIC=/vagrant/spip;

cd "$ROOT_PUBLIC";

if [[ -d "$ROOT_PUBLIC" ]]; then
    echo "$ROOT_PUBLIC exists."
    cd "$ROOT_PUBLIC"
else
    echo "$ROOT_PUBLIC does not exist. Exiting…"
    exit
fi

echo "
`date '+%H:%M'`: Downloading spip from files.spip.net…
=============================================================================="

SPIP_ZIP_URL=https://files.spip.net/spip/stable/spip-3.2.zip?1613419963;

curl --silent --show-error --output spip.zip "$SPIP_ZIP_URL"
unzip -o spip;
rm spip.zip;
